package ca.csf.mobile1.tp1.activity;

import java.io.IOException;

import ca.csf.mobile1.tp1.chemical.compound.ChemicalCompoundFactory;


/**
 * Created by Jeammy on 02/03/17.
 */

public class ComputeActivity {

    private ChemicalCompoundFactory factory;

    public ComputeActivity(ChemicalCompoundFactory factory)throws IOException {
        this.factory = factory;
    }

    /**
     * Calcule le poid d'un element.
     * @param string : Le symbole de l'element sous forme de string.
     * @return Le poid de l'element.
     * @throws Exception : Erreur possible de la string donnee en param.
     */
    public Double calculate(String string)throws Exception{
        return factory.createFromString(string).getWeight();
    }
}
