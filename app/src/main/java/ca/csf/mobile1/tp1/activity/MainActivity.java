package ca.csf.mobile1.tp1.activity;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import ca.csf.mobile1.tp1.R;
import ca.csf.mobile1.tp1.chemical.compound.ChemicalCompoundFactory;
import ca.csf.mobile1.tp1.chemical.compound.EmptyFormulaException;
import ca.csf.mobile1.tp1.chemical.compound.EmptyParenthesisException;
import ca.csf.mobile1.tp1.chemical.compound.IllegalCharacterException;
import ca.csf.mobile1.tp1.chemical.compound.IllegalClosingParenthesisException;
import ca.csf.mobile1.tp1.chemical.compound.MisplacedExponentException;
import ca.csf.mobile1.tp1.chemical.compound.MissingClosingParenthesisException;
import ca.csf.mobile1.tp1.chemical.compound.UnknownChemicalElementException;
import ca.csf.mobile1.tp1.chemical.element.ChemicalElement;
import ca.csf.mobile1.tp1.chemical.element.ChemicalElementRepository;

public class MainActivity extends AppCompatActivity {

    private View rootView;
    private EditText inputEditText;
    private TextView outputTextView;

    private ChemicalElementRepository chemicalElementRepository;
    private ChemicalCompoundFactory chemicalCompoundFactory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rootView = findViewById(R.id.rootView);
        inputEditText = (EditText) findViewById(R.id.inputEditText);
        outputTextView = (TextView) findViewById(R.id.outputTextView);

        if (savedInstanceState != null){
            outputTextView.setText(savedInstanceState.getString("RESULT"));
            inputEditText.setText(savedInstanceState.getString("INPUT"));
        }
        //Lecture du fichier texte.
        try {
            readText();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Restore une instance .
     * @param savedInstanceState
     */
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        outputTextView.setText(savedInstanceState.getString("RESULT"));
    }

    /**
     * Sauvegarde une instance.(Les output et input)
     * @param outState
     */
    @Override
    public void onSaveInstanceState(Bundle outState){
        outState.putString("RESULT",outputTextView.getText().toString());
        outState.putString("INPUT",inputEditText.getText().toString());

        super.onSaveInstanceState(outState);
    }

    /**
     * Lecture du fichier texte.
     * @throws IOException
     */
    private void readText() throws IOException {
        chemicalElementRepository = new ChemicalElementRepository();
        chemicalCompoundFactory = new ChemicalCompoundFactory(chemicalElementRepository);

        //Lecture du fichier .txt "chemicalElementsForTests.txt"
        InputStream is = getResources().openRawResource(R.raw.chemical_elements);
        BufferedReader br = new BufferedReader(new InputStreamReader(is));

        try {
            String line=",";
            while(line != null) {
                //valeur du char actif de la ligne
                int value;
                //information a soutirer de la ligne
                String name = new String();
                String symbol = new String();
                int number=0;
                String numberString = new String();
                double weight=0;
                String weightString = new String();

                //premiere info: le nom de l'element
                while ((char)(value = br.read()) != ',') {
                    char c = (char) value;
                    name += c;
                }
                //deuxieme info: le symbole
                while ((char)(value = br.read()) != ',') {
                    char c = (char) value;
                    symbol += c;
                }
                //troisieme info: son numero
                while ((char)(value = br.read()) != ',') {
                    char c = (char) value;
                    numberString += c;
                }
                number = Integer.parseInt(numberString);

                //quatrieme info: son poid
                while ((char)(value = br.read()) != '\r' && value != -1) {
                    char c = (char) value;
                    weightString += c;
                }
                weight = Double.parseDouble(weightString);

                //saut de ligne.
                line=br.readLine();
                //creation de l'element et ajout dans le repository
                ChemicalElement element = new ChemicalElement(name,symbol,number,weight);
                chemicalElementRepository.add(element);
            }
        }catch (Exception e)
        {
            throw new IOException();
        }
        finally {
            br.close();
            is.close();
        }
    }

    /**
     * Action du click sur le bouton computeButton
     * @param view
     * @throws Exception
     */
    public void onComputeButtonClicked(View view) throws Exception {
		//TODO : Pour l'instant, n'affiche qu'un message d'erreur
        double result=0;

        ComputeActivity compute = new ComputeActivity(chemicalCompoundFactory);
        try {
            //Appel de la fonction pour calculer la masse.
            result = compute.calculate(inputEditText.getText().toString());
        }
        catch (Exception e)
        {
            if (e instanceof EmptyFormulaException){
                Snackbar.make(rootView, R.string.text_empty_formula, Snackbar.LENGTH_LONG).show();
            }
            else if(e instanceof IllegalCharacterException){
                Snackbar.make(rootView, R.string.text_illegal_character, Snackbar.LENGTH_LONG).show();
            }
            else if(e instanceof MisplacedExponentException){
                Snackbar.make(rootView, R.string.text_misplaced_exponent, Snackbar.LENGTH_LONG).show();
            }
            else if(e instanceof EmptyParenthesisException){
                Snackbar.make(rootView, R.string.text_empty_parenthesis, Snackbar.LENGTH_LONG).show();
            }
            else if(e instanceof UnknownChemicalElementException){
                Snackbar.make(rootView, R.string.text_unknown_chemical_element, Snackbar.LENGTH_LONG).show();
            }
            else if(e instanceof MissingClosingParenthesisException){
                Snackbar.make(rootView, R.string.text_missing_closing_parenthesis, Snackbar.LENGTH_LONG).show();
            }
            else if(e instanceof IllegalClosingParenthesisException){
                Snackbar.make(rootView, R.string.text_illegal_closing_parenthesis, Snackbar.LENGTH_LONG).show();
            }
        }
        if (result != 0) {
            outputTextView.setText(String.format(getResources().getString(R.string.text_output), result));
        }
    }

}
